<div>
    @include('components.loading-indicator')

    <div class="card ">

        <div class="card-header">
            <div class="row">
                <div class="col-12">
                    <h4 class="title">Buscador de Mantención</h4>
                </div>
            </div>
        </div>

        <div class="card-body">
            <div class="form-outline">
                <input wire:model.debounce.500ms="search" type="text" class="form-control"
                    placeholder="Ejemplo: ZL8787">
            </div>
        </div>

        <div class="card-footer">
            @if (strlen($search) >= 2)
                <ul class="list-group list-group-flush mb-3">
                    @forelse($searchResults as $result)
                        <li class="list-group-item list-group-item-action"><i class="tim-icons icon-minimal-right"></i>
                            Patente: <a
                                href="{{ route('maintenance.edit', [$result]) }}"><b>{{ $result['patente'] }}</b></a> -
                            RUT:
                            <a href="{{ route('maintenance.edit', [$result]) }}"><b>{{ $result['rut'] }}</b></a>
                        </li>

                    @empty

                        <div class="alert alert-warning alert-with-icon" data-notify="container">
                            <span data-notify="icon" class="tim-icons icon-alert-circle-exc"></span>
                            <span data-notify="message"> No se encontraron resultados.</span>
                        </div>
                    @endforelse
                </ul>
            @endif

            <a href="{{ route('maintenance') }}" style="background: #a10f0f;" class="btn col-12"><i
                class="text-white tim-icons icon-simple-add"></i> Nueva Mantención</a>

            <h5 class="title mt-4">Últimas Mantenciones</h5>
            <div class="table-responsive">
                <table class="table">
                    <thead class="text-primary">
                        <tr>
                            <th class="header">
                                Patente
                            </th>
                            <th class="header">
                                RUT
                            </th>
                            <th class="header">
                                Teléfono
                            </th>
                            <th class="header">
                                Ver Registro
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($ultimasMantenciones as $mantencion)
                        <tr>
                            <td>
                                {{ $mantencion['patente'] }}
                            </td>
                            <td>
                                {{ $mantencion['rut'] }}
                            </td>
                            <td>
                                {{ $mantencion['telefono'] }}
                            </td>
                            <td>
                                <a type="button" rel="tooltip" class="btn btn-info btn-sm btn-round" href="{{ route('maintenance.edit', [$mantencion]) }}">
                                    Acceder
                                    <i class="tim-icons icon-minimal-right"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>

    </div>
</div>
