<?php

namespace App\Http\Livewire;

use App\Models\Vehicle as ModelsVehicle;
use Livewire\Component;
use Livewire\WithPagination;

class CreateVehicle extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $marca, $modelo, $id_vehiculo;

    public $btn = 'Guardar';
    public $btnColor = 'btn-default';

    public $clickBtnCancel = true;

    protected $listeners = ['display-modal' => 'toggleModal', 'delete'];

    //Reglas de validación.
    protected $rules = [
        'marca' => 'required',
        'modelo' => 'required',
    ];

    //Mensajes de validación.
    protected $messages = [
        'marca.required' => 'Por favor ingrese MARCA.',
        'modelo.required' => 'Por favor ingrese MODELO.',
    ];

    //Validación en tiempo real.
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function toggleModal()
    {
        $this->limpiarCampos();

        $this->dispatchBrowserEvent('show-modal-vehicle');
    }

    public function limpiarCampos()
    {

        $this->reset(['id_vehiculo', 'marca', 'modelo', 'btn', 'btnColor', 'clickBtnCancel']);

    }

    public function save()
    {
        $this->validate();

        ModelsVehicle::updateOrCreate(
            ['_id' => $this->id_vehiculo],
            [
                'marca' => strtoupper($this->marca),
                'modelo' => strtoupper($this->modelo)
            ]
        );

        $this->dispatchBrowserEvent('swal-notify', [
            'position' => 'center',
            'icon' => 'success',
            'title' => 'Guardado correctamente.',
            'showConfirmButton' => false,
            'timer' => 2000
        ]);

    }

    public function cancel()
    {

        if($this->clickBtnCancel == true){

            $this->dispatchBrowserEvent('hide-modal-vehicle');

        }else{

            $this->limpiarCampos();

        }
    }

    public function edit($id)
    {
        $vehicle = ModelsVehicle::findOrFail($id);

        $this->id_vehiculo = $id;
        $this->marca = $vehicle->marca;
        $this->modelo = $vehicle->modelo;

        $this->btn = 'Actualizar';
        $this->btnColor = 'btn-info';

        $this->clickBtnCancel = false;
    }

    public function delete(ModelsVehicle $vehicle)
    {
        $vehicle->delete();

        $this->emitTo('maintenance', 'updateVehicle');
    }

    public function updatingMarca()
    {
        $this->resetPage();
    }

    public function updatingModelo()
    {
        $this->resetPage();
    }

    public function render()
    {
        $vehicles = ModelsVehicle::when($this->marca, function ($query) {
            $query->where('marca', 'like', '%' . $this->marca . '%');
        })
            ->when($this->modelo, function ($query) {
                $query->where('modelo', 'like', '%' . $this->modelo . '%');
            })
            ->orderBy('marca')
            ->orderBy('modelo')
            ->paginate(5);

        return view('livewire.create-vehicle', ['vehicles' => $vehicles]);
    }
}
