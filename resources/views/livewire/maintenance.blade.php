<div>
    @include('components.loading-indicator')

    @livewire('create-vehicle')

    @livewire('create-mechanic')


    {{-- Vehículo --}}
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-sm-6 text-left">
                    <h4 class="title">{{ $title }}</h4>
                </div>

                <div class="col-sm-6">
                    @if ($id_maintenance)
                    <div class="btn-group btn-group-toggle float-right" data-toggle="buttons">
                        <label onclick="downloadPDF('{{ $id_maintenance }}')" class="btn btn-sm btn-warning btn-simple active" id="0">
                          <input type="radio" name="options" checked="">
                          <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block">PDF</span>
                          <span class="d-block d-sm-none">
                            <i class="tim-icons icon-cloud-download-93"></i>
                          </span>
                        </label>
                        <label wire:click="$emit('deleteMaintenance', '{{ $id_maintenance }}')"
                            class="btn btn-sm btn-danger btn-simple">
                            <input type="radio" name="options" checked="">
                            <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block">Eliminar</span>
                            <span class="d-block d-sm-none">
                                <i class="tim-icons icon-simple-remove"></i>
                            </span>
                        </label>
                      </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="card-body">
            <div class="form-group @error('patente') has-label has-danger @enderror">
                <label for="patente">Patente</label>
                <input wire:model.defer="patente" type="text" name="patente" class="form-control"
                    style="text-transform:uppercase;" placeholder="ZL8787">

                @error('patente')
                <p class="text-danger">{{ $message }}</p>
                @enderror
            </div>

            <div class="form-group @error('rut') has-label has-danger @enderror">
                <label for="rut">RUT</label>
                <input wire:model.defer="rut" type="text" name="rut" class="form-control" placeholder="12345678-9">

                @error('rut')
                <p class="text-danger">{{ $message }}</p>
                @enderror
            </div>

            <div class="form-group @error('telefono') has-label has-danger @enderror">
                <label for="telefono">Teléfono</label>
                <input wire:model.defer="telefono" type="number" name="telefono" class="form-control">

                @error('telefono')
                <p class="text-danger">{{ $message }}</p>
                @enderror
            </div>

            <div class="form-row">
                <div class="form-group @error('marca') has-label has-danger @enderror col-md-6">
                    <label for="marca"><a href="#" wire:click="$emitTo('create-vehicle', 'display-modal')"><i
                                class="tim-icons icon-pencil"></i> Marca</a></label>
                    <select wire:model="marca" name="marca" class="form-control" required>
                        <option value="">-- seleccionar --</option>
                        @foreach ($vehicles as $vehicle)
                        <option value="{{ $vehicle->marca }}">{{ $vehicle->marca }}</option>
                        @endforeach
                    </select>

                    @error('marca')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group @error('modelo') has-label has-danger @enderror col-md-6">
                    <label for="modelo"><a href="#" wire:click="$emitTo('create-vehicle', 'display-modal')"><i
                                class="tim-icons icon-pencil"></i> Modelo</a></label>
                    <select wire:model.defer="modelo" name="modelo" class="form-control" required>
                        @if ($modelos->count() == 0)
                        <option value="">-- seleccionar marca primero --</option>
                        @endif
                        @foreach ($modelos as $modelo)
                        <option value="{{ $modelo->modelo }}">{{ $modelo->modelo }}</option>
                        @endforeach
                    </select>

                    @error('modelo')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="form-row">
                <div class="form-group @error('kmLlegada') has-label has-danger @enderror col-md-6">
                    <label for="kmLlegada">KM de Llegada</label>
                    <input wire:model.defer="kmLlegada" type="number" min="0" name="kmLlegada" class="form-control">

                    @error('kmLlegada')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>

                <div class="form-group @error('mecanico') has-label has-danger @enderror col-md-6">
                    <label for="mecanico"><a href="#" wire:click="$emitTo('create-mechanic', 'display-modal')"><i
                                class="tim-icons icon-pencil"></i> Mecánico</a></label>
                    <select wire:model="mecanico" name="mecanico" class="form-control" required>
                        <option value="">-- seleccionar --</option>
                        @foreach ($mechanics as $mechanic)
                        <option value="{{ $mechanic->nombre }}">{{ $mechanic->nombre }}</option>
                        @endforeach
                    </select>

                    @error('mecanico')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
            </div>
        </div>

        <div class="card-footer">
            <button wire:click.prevent="save()" type="button" class="btn {{ $btnColor }} col-12">{{ $btn }}</button>
        </div>
    </div>
    {{-- End Vehículo --}}

</div>

@push('js')
<script>
    /* window.addEventListener('swal-notify', event => {

        Swal.fire({
            position: event.detail.position,
            icon: event.detail.icon,
            title: event.detail.title,
            showConfirmButton: event.detail.showConfirmButton,
            timer: event.detail.timer,
        })

    }) */

    Livewire.on('deleteMaintenance', id => {

        Swal.fire({
            title: '¿Estás seguro?',
            //text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Eliminar',
            cancelButtonText: 'Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {

                Livewire.emitTo('maintenance', 'delete', id);

            }
        })

    })

    function downloadPDF(id){
        let url = "{{ route('download.pdf', ':id') }}";
        url = url.replace(':id', id);
        document.location.href=url;
    }

</script>
@endpush
