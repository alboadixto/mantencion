<div>
    @include('components.loading-indicator')

    @livewire('add-service', ['maintenanceResult' => $maintenanceResult])


    <div class="row mb-3">
        <div class="col-sm-6 text-left">
            <h4 class="card-title">Servicios realizados ({{$countServices}})</h4>
        </div>
        <div class="col-sm-6">
            <div class="btn-group btn-group-toggle float-right" data-toggle="buttons">
                <label wire:click="$emitTo('add-service', 'display-modal')"
                    class="btn btn-sm btn-primary btn-simple active">
                    <input type="radio" name="options" checked="">
                    <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block">Agregar</span>
                    <span class="d-block d-sm-none">
                        <i class="tim-icons icon-simple-add"></i>
                    </span>
                </label>
            </div>
        </div>
    </div>


    @if($servicesResults)

    <div class="col-md-12 mb-5">
        <div class="card ">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table tablesorter" id="simple-table">
                        <thead class="text-primary">
                            <tr>
                                <th class="header">
                                    Servicio
                                </th>
                                <th class="header">
                                    Intervalo Mantención
                                </th>
                                <th class="header">
                                    Descripción
                                </th>
                                <th class="header">
                                    Fecha
                                </th>
                                <th class="header">
                                    Mecánico
                                </th>
                                <th class="header">
                                    Próxima Mantención
                                </th>
                                <th class="text-center header">
                                    Acciones
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($servicesResults as $service)

                            @php
                            $proxMantencion = number_format($service['proxMantencion'],0,",",".");
                            $intMantencion = number_format($service['intMantencion'],0,",",".");


                            $color = 'table-success';

                            if ($service['proxMantencion'] < $kmLlegada) {
                            $color = 'table-danger';
                            }
                            @endphp
                            <tr class="{{ $color }}">
                                <td>
                                    {{ $service['nombre'] }}
                                </td>
                                <td>
                                    {{ $intMantencion }} KM
                                </td>
                                <td>
                                    {{ $service['descripcion'] }}
                                </td>
                                <td>
                                    {{ \Carbon\Carbon::parse($service['updated_at'])->diffForHumans() }}
                                </td>
                                <td>
                                    {{ $service['mecanico'] }}
                                </td>
                                <td>
                                    {{ $proxMantencion }} KM
                                </td>
                                <td class="text-center">
                                    <div class="row" style="width: 190px">
                                        <div class="col-4">
                                            <button
                                                wire:click="$emitTo('add-service', 'edit', '{{ $service['nombre'] }}', '{{ $service['proxMantencion'] }}', '{{ $service['intMantencion'] }}', '{{ $service['descripcion'] }}', '{{ $service['mecanico'] }}', '{{ $service['token'] }}')"
                                                class="btn btn-info btn-icon btn-round animation-on-hover" type="button"><i
                                                    class="tim-icons icon-pencil"></i>
                                            </button>
                                        </div>

                                        <div class="col-4">
                                            <button wire:click="$emit('reset', '{{ $service['token'] }}', '{{ $service['intMantencion'] }}', '{{ $kmLlegada }}')"
                                                class="btn btn-default btn-icon btn-round animation-on-hover" type="button"><i
                                                    class="tim-icons icon-refresh-02"></i>
                                            </button>
                                        </div>

                                        <div class="col-4">
                                            <button wire:click="$emit('deleteService', '{{ $service['token'] }}')"
                                                class="btn btn-danger btn-icon btn-round animation-on-hover" type="button"><i
                                                    class="tim-icons icon-trash-simple"></i>
                                            </button>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @else

    <div class="alert alert-primary alert-with-icon" data-notify="container"
        wire:click="$emitTo('add-service', 'display-modal')">
        <span data-notify="icon" class="tim-icons icon-alert-circle-exc"></span>
        <span data-notify="message"> No ha realizado servicios. Haga click en <b>Agregar</b></span>
    </div>

    @endIf


</div>

@push('js')
<script>
    Livewire.on('deleteService', token => {

        Swal.fire({
            title: '¿Estás seguro?',
            //text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Eliminar',
            cancelButtonText: 'Cancelar',
        }).then((result) => {
            if (result.isConfirmed) {

                Livewire.emitTo('add-service', 'delete', token);

            }
        })

    })


    Livewire.on('reset', (token, intMantencion, kmLlegada) => {

        Swal.fire({
            title: '¿Estás seguro?',
            //text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Reset',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.isConfirmed) {

                Livewire.emitTo('add-service', 'resetProxMantencion', token, intMantencion, kmLlegada);

            }
        })

    })

</script>
@endpush
