<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use App\Models\Maintenance as ModelsMaintenance;

class PdfController extends Controller
{
    public function download($id)
    {
        $maintenanceResult = ModelsMaintenance::findOrFail($id);

        $servicesResults = $maintenanceResult['servicios'];
        $kmLlegada = $maintenanceResult['kmLlegada'];

        $dompdf = PDF::loadView('pdf', ['maintenanceResult' => $maintenanceResult, 'servicesResults' => $servicesResults, 'kmLlegada' => $kmLlegada]);

        $namePDF = date("dmY").'_'.$maintenanceResult['patente'].'.pdf'; //28032022_CHVH19.pdf

        //$customPaper = array(0,0,1920,360);

        return $dompdf->setPaper('letter')->download($namePDF);
    }
}
