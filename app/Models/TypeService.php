<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
//use Illuminate\Database\Eloquent\Model;

use Jenssegers\Mongodb\Eloquent\Model as Model;

class TypeService extends Model
{
    use HasFactory;

    protected $primaryKey = '_id';

    protected $fillable = [
        'nombre',
        'prox_mantencion',
        'int_mantencion'
    ];

    protected $collection = 'types-services';

    public $timestamps = false;
}
