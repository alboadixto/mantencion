@extends('layouts.app', ['pageSlug' => 'maintenance'])

@section('css')
{{-- aqui van los estilos --}}
@endsection

@section('content')
<div class="row">
    <div class="col-12">

        <livewire:maintenance>

    </div>
</div>
@endsection

@push('js')
{{-- aqui van los js --}}
@endpush
