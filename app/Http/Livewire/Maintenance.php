<?php

namespace App\Http\Livewire;

use App\Models\Maintenance as ModelsMaintenance;
use App\Models\Vehicle as ModelsVehicle;
use App\Models\Mechanic as ModelsMechanic;
use App\Models\TypeService as ModelsTypeService;
use Livewire\Component;
use ValidateRequests;
use Freshwork\ChileanBundle\Rut;

class Maintenance extends Component
{
    public $title = 'Nueva Mantención';
    public $btn = 'Ingresar Mantención';
    public $btnColor = 'btn-primary';

    public $id_maintenance;

    public $patente, $rut, $marca, $modelo, $kmLlegada, $telefono, $mecanico;

    public $vehicles, $modelos, $mechanics;

    protected $listeners = ['updateVehicle', 'delete', 'updateMechanic'];

    //Reglas de validación.
    protected $rules = [
        'patente' => 'required',
        'rut' => 'required|cl_rut',
        'telefono' => 'required',
        'marca' => 'required',
        'modelo' => 'required',
        'kmLlegada' => 'required|integer|numeric|min:0',
        'mecanico' => 'required',
    ];

    //Mensajes de validación.
    protected $messages = [
        'patente.required' => 'Por favor ingrese PATENTE.',
        'rut.required' => 'Por favor ingrese RUT.',
        'rut.cl_rut' => 'Por favor ingrese RUT VÁLIDO.',
        'telefono.required' => 'Por favor ingrese TELÉFONO',
        'marca.required' => 'Por favor ingrese MARCA.',
        'modelo.required' => 'Por favor ingrese MODELO.',
        'kmLlegada.required' => 'Por favor ingrese KM DE LLEGADA.',
        'kmLlegada.integer' => 'Por favor ingrese NUMEROS SIN DECIMALES.',
        'kmLlegada.numeric' => 'Por favor ingrese SOLO NUMEROS.',
        'kmLlegada.min' => 'Por favor ingrese NUMEROS MAYOR O IGUALES A 0.',
        'mecanico.required' => 'Por favor ingrese MECÁNICO.',
    ];

    //Validación en tiempo real.
    /* public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    } */

    public function mount($maintenanceResult = null)
    {
        $this->vehicles = ModelsVehicle::orderBy('marca')->get()->unique('marca');
        $this->mechanics = ModelsMechanic::orderBy('nombre')->get()->unique('nombre');
        $this->modelos = collect();

        if ($maintenanceResult) {

            $this->title = 'Editar Mantención';
            $this->btn = 'Actualizar';
            $this->btnColor = 'btn-info';

            $this->id_maintenance = $maintenanceResult->id;
            $this->patente = $maintenanceResult->patente;
            $this->rut = $maintenanceResult->rut;
            $this->telefono = $maintenanceResult->telefono;
            $this->marca = $maintenanceResult->marca;
            $this->modelos = ModelsVehicle::where('marca', $maintenanceResult->marca)->orderBy('modelo')->get();
            $this->modelo = $maintenanceResult->modelo;
            $this->kmLlegada = number_format($maintenanceResult->kmLlegada, 0, ",", ".");
            $this->mecanico = $maintenanceResult->mecanico;
        }
    }

    public function render()
    {
        return view('livewire.maintenance');
    }

    //evento proveniente de create-vehicle.blade.php al momento de guardar/eliminar un nuevo vehículo
    public function updateVehicle()
    {
        $this->vehicles = ModelsVehicle::orderBy('marca')->get()->unique('marca');
    }

    public function updatedMarca($value)
    {
        $this->modelos = ModelsVehicle::where('marca', $value)->orderBy('modelo')->get();
        $this->modelo = $this->modelos->first()->modelo ?? '';
    }

    //evento proveniente de create-mechanic.blade.php al momento de guardar/eliminar un nuevo mecánico
    public function updateMechanic()
    {
        $this->mechanics = ModelsMechanic::orderBy('nombre')->get()->unique('nombre');
    }

    public function save()
    {
        $this->kmLlegada = preg_replace('/[^0-9]/', '', $this->kmLlegada);
        $this->validate();

        /* Insertar en base de datos */
        $maintenance = ModelsMaintenance::updateOrCreate(
            ['_id' => $this->id_maintenance],
            [
                'patente' => strtoupper($this->patente),
                'rut' => Rut::parse($this->rut)->format(Rut::FORMAT_WITH_DASH), //return 12345678-5
                'telefono' => $this->telefono,
                'marca' => $this->marca,
                'modelo' => $this->modelo,
                'kmLlegada' => $this->kmLlegada,
                'mecanico' => $this->mecanico
            ]
        );

        if($this->btn != 'Actualizar'){

            /* Servicios */
            $servicios = array();
            $types_services = ModelsTypeService::all();

            foreach($types_services as $type){

                $servicio = array(
                    'token' => sha1(mt_rand(1, 90000) . date('Y-m-d H:i:s')),
                    'nombre' => $type['nombre'],
                    'proxMantencion' => $type['prox_mantencion'],
                    'intMantencion' => $type['int_mantencion'],
                    'descripcion' => '',
                    'mecanico' => $this->mecanico,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );

                array_push($servicios, $servicio);
            }

            $maintenance->push('servicios', $servicios);
        }

        /* $this->dispatchBrowserEvent('swal-notify', [
            'position' => 'center',
            'icon' => 'success',
            'title' => 'Guardado correctamente.',
            'showConfirmButton' => false,
            'timer' => 2000
        ]); */

        return redirect()->route('maintenance.edit', [$maintenance])->with('success', 'Guardado correctamente.');
    }

    public function delete(ModelsMaintenance $id_maintenance)
    {
        $id_maintenance->delete();

        return redirect()->route('search');
    }
}
