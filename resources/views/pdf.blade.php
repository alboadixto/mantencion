<!doctype html>
<html lang="en">

<head>
    <title>Mantenciones</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
        table {
            font-size: 1px;
        }

        @page {
            margin-top: 1cm;
            margin-bottom: 1cm;
            margin-right: 0cm;
            margin-left: 0cm;
        }

        .form-group label {
            font-size: 5px;
        }
    </style>
</head>

<body>
    <div class="container">
        <h5 class=" font-weight-bold">Mantención Realizada</h5>

        <form>
            <div class="form-group row">
              <label class="col-sm-12 col-form-label">Patente: {{ $maintenanceResult->patente }}</label>
              <label class="col-sm-12 col-form-label">RUT: {{ $maintenanceResult->rut }}</label>
              <label class="col-sm-12 col-form-label">Teléfono: {{ $maintenanceResult->telefono }}</label>
              <label class="col-sm-12 col-form-label">Marca: {{ $maintenanceResult->marca }} - Modelo: {{ $maintenanceResult->modelo }}</label>
              <label class="col-sm-12 col-form-label">KM de Llegada: {{ number_format($maintenanceResult->kmLlegada,0,",",".") }}</label>
              <label class="col-sm-12 col-form-label">Mecánico: {{ $maintenanceResult->mecanico }}</label>
            </div>
        </form>


        <table class="table table-bordered mt-5">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">SERVICIO</th>
                    <th scope="col">INT.MANTENCIÓN</th>
                    <th scope="col">DESCRIPCION</th>
                    <th scope="col">FECHA</th>
                    <th scope="col">MECANICO</th>
                    <th scope="col">PROX.MANTENCIÓN</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($servicesResults as $service)

                @php
                    $proxMantencion = number_format($service['proxMantencion'],0,",",".");
                    $intMantencion = number_format($service['intMantencion'],0,",",".");

                    $color = 'table-success';

                    if ($service['proxMantencion'] < $kmLlegada) {
                        $color = 'table-danger';
                    }

                @endphp

                <tr class="{{ $color }}">
                    <td>{{ $service['nombre'] }}</td>
                    <td>{{ $intMantencion }} KM</td>
                    <td>{{ $service['descripcion'] }}</td>
                    <td>{{ \Carbon\Carbon::parse($service['updated_at'])->isoFormat('DD/MM/YY') }}</td>
                    <td>{{ $service['mecanico'] }}</td>
                    <td>{{ $proxMantencion }} KM</td>
                </tr>
                @empty

                @endforelse
            </tbody>
        </table>
    </div>
</body>

</html>
