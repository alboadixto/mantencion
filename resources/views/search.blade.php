@extends('layouts.app', ['pageSlug' => 'search'])

@section('css')
{{-- aqui van los estilos --}}
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">

        <livewire:search-dropdown>

    </div>
</div>
@endsection

@push('js')
{{-- aqui van los js --}}
@endpush
