<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
//use Illuminate\Database\Eloquent\Model;

use Jenssegers\Mongodb\Eloquent\Model as Model;

class Vehicle extends Model
{
    use HasFactory;

    protected $primaryKey = '_id';

    protected $fillable = [
        'marca',
        'modelo',
    ];

    protected $collection = 'vehicles';

    public $timestamps = false;
}
