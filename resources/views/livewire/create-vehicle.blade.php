<div>
    <div wire:ignore.self class="modal fade" tabindex="-1" role="dialog" id="vehicle-modal"
        aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered " role="document"
            style="margin-bottom : 100px; margin-top: -150px">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Ingresar Marca y Modelo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="tim-icons icon-simple-remove"></i>
                      </button>
                </div>

                <div class="modal-body">
                    <div class="form-group @error('marca') has-label has-danger @enderror">
                        <label>Marca</label>
                        <input wire:model.debounce.500ms="marca" type="text" name="marca" class="form-control"
                            style="text-transform:uppercase;" placeholder="NISSAN">

                        @error('marca')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>

                    <div class="form-group @error('modelo') has-label has-danger @enderror">
                        <label>Modelo</label>
                        <input wire:model.debounce.500ms="modelo" type="text" name="modelo" class="form-control"
                            style="text-transform:uppercase;" placeholder="QASHQAI">

                        @error('modelo')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>

                    @if ($btn != 'Actualizar')
                    <table class="table table-sm table-hover">
                        <thead>
                            <tr class="d-flex">
                                <th class="col-sm-4 col-md-3" scope="col">Acciones</th>
                                <th class="col-sm-4 col-md-4" scope="col">Marca</th>
                                <th class="col-sm-4 col-md-5" scope="col">Modelo</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($vehicles as $vehicle)
                            <tr class="d-flex">
                                <td class="col-sm-4 col-md-3">
                                    <div class="button-container">
                                        <button wire:click.prevent="edit('{{$vehicle->id}}')"
                                            class="btn btn-info btn-icon btn-round">
                                            <i class="tim-icons icon-pencil"></i>
                                        </button>
                                        <button wire:click.prevent="$emit('deleteVehicle', '{{$vehicle->id}}')"
                                            class="btn btn-danger btn-icon btn-round">
                                            <i class="tim-icons icon-trash-simple"></i>
                                        </button>
                                    </div>
                                </td>
                                <td class="col-sm-4 col-md-4">{{$vehicle->marca}}</td>
                                <td class="col-sm-4 col-md-5">{{$vehicle->modelo}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    @if ($vehicles->hasPages())
                    <div class="float-right">
                        {{$vehicles->links()}}
                    </div>
                    @endif
                    @endif
                </div>

                <div class="modal-footer">
                    <button wire:click.prevent="save()" type="button" class="btn {{$btnColor}}">{{$btn}}</button>
                    <button wire:click.prevent="cancel()" type="button" class="btn btn-danger">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
</div>

@push('js')
<script>
    window.addEventListener('show-modal-vehicle', () => {
        $('#vehicle-modal').modal('show');
    })

    window.addEventListener('hide-modal-vehicle', () => {
        $('#vehicle-modal').modal('hide');
    })

    Livewire.on('deleteVehicle', id => {

        Swal.fire({
            title: '¿Estás seguro?',
            //text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Eliminar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
        if (result.isConfirmed) {

            Livewire.emitTo('create-vehicle', 'delete', id);

        }
        })

    })

    window.addEventListener('swal-notify', event => {
        $('#vehicle-modal').modal('hide');

        Swal.fire({
            position: event.detail.position,
            icon: event.detail.icon,
            title: event.detail.title,
            showConfirmButton: event.detail.showConfirmButton,
            timer: event.detail.timer,
        })

        Livewire.emitTo('maintenance', 'updateVehicle');
    })
</script>
@endpush
