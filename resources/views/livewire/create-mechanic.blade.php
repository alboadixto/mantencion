<div>
    <div wire:ignore.self class="modal fade" tabindex="-1" role="dialog" id="mechanic-modal"
        aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered " role="document"
            style="margin-bottom : 100px; margin-top: -150px">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Ingresar Mecánico</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="tim-icons icon-simple-remove"></i>
                      </button>
                </div>

                <div class="modal-body">
                    <div class="form-group @error('nombre') has-label has-danger @enderror">
                        <label>Nombre</label>
                        <input wire:model.debounce.500ms="nombre" type="text" name="nombre" class="form-control"
                            style="text-transform:uppercase;" placeholder="Juan Pérez">

                        @error('nombre')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>

                    @if ($btn != 'Actualizar')
                    <table class="table table-sm table-hover">
                        <thead>
                            <tr class="d-flex">
                                <th class="col-sm-4 col-md-3" scope="col">Acciones</th>
                                <th class="col-sm-8 col-md-9" scope="col">Nombre</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($mechanics as $mechanic)
                            <tr class="d-flex">
                                <td class="col-sm-4 col-md-3">
                                    <div class="button-container">
                                        <button wire:click.prevent="edit('{{$mechanic->id}}')"
                                            class="btn btn-info btn-icon btn-round">
                                            <i class="tim-icons icon-pencil"></i>
                                        </button>
                                        <button wire:click.prevent="$emit('deleteMechanic', '{{$mechanic->id}}')"
                                            class="btn btn-danger btn-icon btn-round">
                                            <i class="tim-icons icon-trash-simple"></i>
                                        </button>
                                    </div>
                                </td>
                                <td class="col-sm-4 col-md-4">{{$mechanic->nombre}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    @if ($mechanics->hasPages())
                    <div class="float-right">
                        {{$mechanics->links()}}
                    </div>
                    @endif
                    @endif
                </div>

                <div class="modal-footer">
                    <button wire:click.prevent="save()" type="button" class="btn {{$btnColor}}">{{$btn}}</button>
                    <button wire:click.prevent="cancel()" type="button" class="btn btn-danger">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
</div>

@push('js')
<script>
    window.addEventListener('show-modal-mechanic', () => {
        $('#mechanic-modal').modal('show');
    })

    window.addEventListener('hide-modal-mechanic', () => {
        $('#mechanic-modal').modal('hide');
    })

    Livewire.on('deleteMechanic', id => {

        Swal.fire({
            title: '¿Estás seguro?',
            //text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Eliminar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
        if (result.isConfirmed) {

            Livewire.emitTo('create-mechanic', 'delete', id);

        }
        })

    })

    window.addEventListener('swal-notify', event => {
        $('#mechanic-modal').modal('hide');

        Swal.fire({
            position: event.detail.position,
            icon: event.detail.icon,
            title: event.detail.title,
            showConfirmButton: event.detail.showConfirmButton,
            timer: event.detail.timer,
        })

        Livewire.emitTo('maintenance', 'updateMechanic');
    })
</script>
@endpush
