<footer class="footer">
    <div class="copyright">
        &copy; {{ now()->year }} {{ __('made with') }} <i class="tim-icons icon-heart-2"></i> {{ __('by') }}
        <a href="https://entercode.cl/" target="_blank">{{ __('Entercode') }}</a>.
    </div>
    </div>
</footer>
