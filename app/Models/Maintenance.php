<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
//use Illuminate\Database\Eloquent\Model;

use Jenssegers\Mongodb\Eloquent\Model as Model;

class Maintenance extends Model
{
    use HasFactory;

    protected $primaryKey = '_id';

    /* protected $fillable = [
        'patente',
        'rut',
        'marca',
        'modelo',
        'kmInicial',
        'kmActual',
        'servicio',
    ]; */

    protected $guarded = ['_id'];

    protected $collection = 'maintenances';
}
