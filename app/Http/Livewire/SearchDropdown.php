<?php

namespace App\Http\Livewire;

use App\Models\Maintenance as ModelsMaintenance;
use Livewire\Component;

class SearchDropdown extends Component
{
    public $search = '';

    public function render()
    {

        $searchResults = [];

        if(strlen($this->search) >= 2){
            $searchResults = ModelsMaintenance::where('patente', 'LIKE', '%' . $this->search . '%')
            ->orWhere('rut', 'LIKE', '%' . $this->search . '%')
            ->get();
        }

        return view('livewire.search-dropdown', [
            'searchResults' => collect($searchResults)->take(7),
            'ultimasMantenciones' => ModelsMaintenance::orderBy('updated_at', 'desc')->take(20)->get(),
        ]);
    }
}
