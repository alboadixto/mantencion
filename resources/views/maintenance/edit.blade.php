@extends('layouts.app', ['pageSlug' => 'maintenance'])

@push('css')
{{-- aqui van los estilos --}}
@endpush

@section('content')
<div class="row">
    <div class="col-12">

        <livewire:maintenance :maintenanceResult="$maintenanceResult">

        <livewire:service :maintenanceResult="$maintenanceResult">


    </div>
</div>
@endsection

@push('js')

@if (session('success'))
<script>

    Swal.fire({
            position: 'center',
            icon: 'success',
            title: "{{session('success')}}",
            showConfirmButton: false,
            timer: 1500,
        })

</script>
@endif

@endpush
