<div>
    <div wire:ignore.self class="modal fade" tabindex="-1" role="dialog" id="service-modal"
        aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered " role="document"
            style="margin-bottom : 100px; margin-top: -150px">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ $title }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="tim-icons icon-simple-remove"></i>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label for="nombre">Servicio</label>
                        <input wire:model.defer="nombre" type="text" name="nombre" class="form-control">

                        @error('nombre')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                        {{-- <label for="nombre">Nombre</label>
                        <select wire:model.defer="nombre" name="nombre" class="form-control" required>
                            @foreach ($nombreServicios as $servicio)
                            <option value="{{ $servicio->nombre }}">{{ $servicio->nombre }}</option>
                            @endforeach
                            <option value="Otro">-- Otro --</option>
                        </select> --}}
                    </div>

                    <div class="form-group @error('proxMantencion') has-label has-danger @enderror">
                        <label for="proxMantencion">Próxima Mantención</label>
                        <input wire:model.defer="proxMantencion" type="number" min="0" name="proxMantencion"
                            class="form-control">

                        @error('proxMantencion')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>

                    <div class="form-group @error('intMantencion') has-label has-danger @enderror">
                        <label for="intMantencion">Intervalo Mantención</label>
                        <input wire:model.defer="intMantencion" type="number" min="0" name="intMantencion"
                            class="form-control">

                        @error('intMantencion')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="descripcion">Descripción</label>
                        <input wire:model.defer="descripcion" type="text" name="descripcion"
                            class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="descripcion">Mecánico</label>

                        <select wire:model="mecanico" name="mecanico" class="form-control" required>
                            @foreach ($mechanics as $mechanic)
                            <option value="{{ $mechanic->nombre }}">{{ $mechanic->nombre }}</option>
                            @endforeach
                        </select>
                    </div>

                </div>

                <div class="modal-footer">
                    <button wire:click.prevent="save()" type="button" class="btn {{ $btnColor }}">{{ $btn }}</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
</div>

@push('js')
<script>
    window.addEventListener('show-modal-service', () => {
        $('#service-modal').modal('show');
    })

    /*Livewire.on('hide-modal-service', () => {
        $('#service-modal').modal('hide');
    }) */

    window.addEventListener('swal-notify', event => {
        $('#service-modal').modal('hide');

        Swal.fire({
            position: event.detail.position,
            icon: event.detail.icon,
            title: event.detail.title,
            showConfirmButton: event.detail.showConfirmButton,
            timer: event.detail.timer,
        })

        Livewire.emitTo('service', 'updateService');
    })
</script>
@endpush
