<?php

namespace App\Http\Livewire;

use App\Models\Mechanic as ModelsMechanic;
use Livewire\Component;
use Livewire\WithPagination;

class CreateMechanic extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $nombre, $id_mecanico;

    public $btn = 'Guardar';
    public $btnColor = 'btn-default';

    public $clickBtnCancel = true;

    protected $listeners = ['display-modal' => 'toggleModal', 'delete'];

    //Reglas de validación.
    protected $rules = [
        'nombre' => 'required',
    ];

    //Mensajes de validación.
    protected $messages = [
        'nombre.required' => 'Por favor ingrese NOMBRE.',
    ];

    //Validación en tiempo real.
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function toggleModal()
    {
        $this->limpiarCampos();

        $this->dispatchBrowserEvent('show-modal-mechanic');
    }

    public function limpiarCampos()
    {

        $this->reset(['id_mecanico', 'nombre', 'btn', 'btnColor', 'clickBtnCancel']);

    }

    public function save()
    {
        $this->validate();

        ModelsMechanic::updateOrCreate(
            ['_id' => $this->id_mecanico],
            [
                'nombre' => strtoupper($this->nombre)
            ]
        );

        $this->dispatchBrowserEvent('swal-notify', [
            'position' => 'center',
            'icon' => 'success',
            'title' => 'Guardado correctamente.',
            'showConfirmButton' => false,
            'timer' => 2000
        ]);

    }

    public function cancel()
    {

        if($this->clickBtnCancel == true){

            $this->dispatchBrowserEvent('hide-modal-mechanic');

        }else{

            $this->limpiarCampos();

        }
    }

    public function edit($id)
    {
        $mechanic = ModelsMechanic::findOrFail($id);

        $this->id_mecanico = $id;
        $this->nombre = $mechanic->nombre;

        $this->btn = 'Actualizar';
        $this->btnColor = 'btn-info';

        $this->clickBtnCancel = false;
    }

    public function delete(ModelsMechanic $mechanic)
    {
        $mechanic->delete();

        $this->emitTo('maintenance', 'updateMechanic');
    }

    public function updatingNombre()
    {
        $this->resetPage();
    }

    public function render()
    {
        $mechanic = ModelsMechanic::when($this->nombre, function ($query) {
            $query->where('nombre', 'like', '%' . $this->nombre . '%');
        })
            ->orderBy('nombre')
            ->paginate(5);

        return view('livewire.create-mechanic', ['mechanics' => $mechanic]);
    }
}
