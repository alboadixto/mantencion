<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
//use Illuminate\Database\Eloquent\Model;

use Jenssegers\Mongodb\Eloquent\Model as Model;

class Mechanic extends Model
{
    use HasFactory;

    protected $primaryKey = '_id';

    protected $fillable = [
        'nombre',
    ];

    protected $collection = 'mechanics';

    public $timestamps = false;
}

