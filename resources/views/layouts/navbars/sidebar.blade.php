<div class="sidebar">
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="#" class="simple-text logo-mini">{{ __('MT') }}</a>
            <a href="#" class="simple-text logo-normal">{{ __('Mantenciones') }}</a>
        </div>
        <ul class="nav">
            <li @if ($pageSlug == 'search') class="active " @endif>
                <a href="{{ route('search') }}">
                    <i class="tim-icons icon-zoom-split"></i>
                    <p>{{ __('Buscador') }}</p>
                </a>
            </li>
            <li @if ($pageSlug == 'maintenance') class="active " @endif>
                <a href="{{ route('maintenance') }}">
                    <i class="tim-icons icon-settings"></i>
                    <p>{{ __('Mantención') }}</p>
                </a>
            </li>
        </ul>
    </div>
</div>
