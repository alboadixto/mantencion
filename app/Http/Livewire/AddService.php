<?php

namespace App\Http\Livewire;

use App\Models\TypeService as ModelsTypeService;
use App\Models\Maintenance as ModelsMaintenance;
use App\Models\Mechanic as ModelsMechanic;
use Livewire\Component;

class AddService extends Component
{
    public $nombre, $proxMantencion, $intMantencion, $descripcion, $mecanico, $token;

    public $mechanics;

    public $title = 'Agregar Servicio';
    public $btn = 'Guardar';
    public $btnColor = 'btn-primary';

    public $maintenance;

    //public $nombreServicios;

    protected $listeners = ['display-modal' => 'toggleModal', 'edit', 'delete', 'resetProxMantencion'];

    //Reglas de validación.
    protected $rules = [
        'nombre' => 'required',
        'proxMantencion' => 'required|integer|numeric|min:0',
        'intMantencion' => 'required|integer|numeric|min:0',
        'mecanico' => 'required',
    ];

    //Mensajes de validación.
    protected $messages = [
        'nombre.required' => 'Por favor ingrese NOMBRE DEL SERVICIO.',
        'proxMantencion.required' => 'Por favor ingrese PRÓXIMA MANTENCIÓN.',
        'proxMantencion.integer' => 'Por favor ingrese NUMEROS SIN DECIMALES.',
        'proxMantencion.numeric' => 'Por favor ingrese SOLO NUMEROS.',
        'proxMantencion.min' => 'Por favor ingrese NUMEROS MAYOR O IGUALES A 0.',
        'intMantencion.required' => 'Por favor ingrese INTERVALO MANTENCIÓN.',
        'intMantencion.integer' => 'Por favor ingrese NUMEROS SIN DECIMALES.',
        'intMantencion.numeric' => 'Por favor ingrese SOLO NUMEROS.',
        'intMantencion.min' => 'Por favor ingrese NUMEROS MAYOR O IGUALES A 0.',
        'mecanico.required' => 'Por favor ingrese MECÁNICO.',
    ];

    //Validación en tiempo real.
    /* public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    } */

    public function mount($maintenanceResult)
    {

        $this->maintenance = $maintenanceResult; //Valor proveniente de la vista service.blade.php

        $this->mechanics = ModelsMechanic::orderBy('nombre')->get()->unique('nombre');

        /* $this->nombreServicios = ModelsTypeService::orderBy('nombre')->get();
        $this->nombre = $this->nombreServicios->first()->nombre ?? ''; */
    }

    public function toggleModal()
    {
        $this->limpiarCampos();

        $this->dispatchBrowserEvent('show-modal-service');
    }

    public function resetProxMantencion($token, $intMantencion, $kmLlegada)
    {

        $proxMantencion = ((int)$intMantencion + (int)$kmLlegada);

        $this->maintenance->where('servicios.token', $token)->update([
            'servicios.$.proxMantencion' => $proxMantencion,
            'servicios.$.updated_at' => date('Y-m-d H:i:s'),
        ]);

        $this->emitTo('service', 'updateService');

    }

    public function edit($nombre = null, $proxMantencion = null, $intMantencion = null, $descripcion = null, $mecanico = null, $token = null)
    {

        $this->dispatchBrowserEvent('show-modal-service');

        $this->nombre = $nombre;
        $this->proxMantencion = $proxMantencion;
        $this->intMantencion = $intMantencion;
        $this->descripcion = $descripcion;
        $this->mecanico = $mecanico;
        $this->token = $token;

        $this->title = 'Editar Servicio';
        $this->btn = 'Actualizar';
        $this->btnColor = 'btn-info';

    }

    public function delete($token)
    {
        $this->maintenance->where('servicios.token', $token)->pull('servicios', [
            'token' => $token,
        ]);

        $this->emitTo('service', 'updateService');
    }

    public function limpiarCampos()
    {
        $this->reset(['proxMantencion', 'intMantencion', 'descripcion', 'mecanico', 'title', 'btn', 'btnColor', 'token']);
    }

    public function save()
    {
        $this->validate();

        //editar servicio
        if ($this->token) {
            $this->maintenance->where('servicios.token', $this->token)->update([
                'servicios.$.nombre' => $this->nombre,
                'servicios.$.proxMantencion' => $this->proxMantencion,
                'servicios.$.intMantencion' => $this->intMantencion,
                'servicios.$.descripcion' => $this->descripcion,
                'servicios.$.mecanico' => $this->mecanico,
                'servicios.$.updated_at' => date('Y-m-d H:i:s'),

            ]);

        } else {
            //agregar servicio
            $result = array(
                'token' => sha1(mt_rand(1, 90000) . date('Y-m-d H:i:s')),
                'nombre' => $this->nombre,
                'proxMantencion' => $this->proxMantencion,
                'intMantencion' => $this->intMantencion,
                'descripcion' => $this->descripcion,
                'mecanico' => $this->mecanico,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );

            $this->maintenance->push('servicios', [$result]);
        }

        $this->dispatchBrowserEvent('swal-notify', [
            'position' => 'center',
            'icon' => 'success',
            'title' => 'Guardado correctamente.',
            'showConfirmButton' => false,
            'timer' => 2000
        ]);

    }

    public function render()
    {
        return view('livewire.add-service');
    }
}
