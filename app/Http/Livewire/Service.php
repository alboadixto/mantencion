<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Maintenance as ModelsMaintenance;

class Service extends Component
{
    public $idMaintenance;

    public $maintenanceResult;
    public $servicesResults;

    public $countServices;

    public $kmLlegada;

    protected $listeners = ['updateService'];

    public function mount($maintenanceResult)
    {

        $this->maintenanceResult = $maintenanceResult;
        $this->kmLlegada = $this->maintenanceResult['kmLlegada'];
        $this->servicesResults = $this->maintenanceResult['servicios'];
        $this->countServices = count($this->servicesResults);
    }

    //evento proveniente de add-service.blade.php al momento de crear un nuevo servicio
    public function updateService()
    {
        $this->servicesResults = $this->maintenanceResult['servicios'];

        $this->countServices = count($this->servicesResults);
    }

    public function render()
    {
        return view('livewire.service');
    }
}
