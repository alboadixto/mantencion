<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Maintenance as ModelsMaintenance;

class MaintenanceController extends Controller
{
    public function index()
    {
        return view('maintenance.index');
    }

    public function edit($maintenance)
    {

        $maintenanceResult = ModelsMaintenance::findOrFail($maintenance);

        return view('maintenance.edit', compact('maintenanceResult'));
    }
}
